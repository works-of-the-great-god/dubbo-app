package com.wangsong.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wangsong.system.entity.Role;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2021-09-18
 */
public interface RoleMapper extends BaseMapper<Role> {

}
