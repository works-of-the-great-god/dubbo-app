package com.wangsong.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wangsong.system.entity.UserRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2021-09-18
 */
public interface IUserRoleService extends IService<UserRole> {

}
