package com.wangsong.order.service;

import com.wangsong.order.entity.ProductsHistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2021-09-25
 */
public interface IProductsHistoryService extends IService<ProductsHistory> {

}
