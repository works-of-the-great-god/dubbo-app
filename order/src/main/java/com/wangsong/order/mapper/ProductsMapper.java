package com.wangsong.order.mapper;

import com.wangsong.order.entity.Products;
import com.wangsong.order.entity.ProductsES;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2021-09-25
 */
public interface ProductsMapper extends BaseMapper<Products> {

}
