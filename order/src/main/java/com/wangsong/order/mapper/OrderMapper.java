package com.wangsong.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wangsong.order.entity.OrderInfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2021-09-25
 */
public interface OrderMapper extends BaseMapper<OrderInfo> {

}
